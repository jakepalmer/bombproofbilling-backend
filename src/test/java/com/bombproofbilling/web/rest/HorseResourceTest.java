package com.bombproofbilling.web.rest;

import com.bombproofbilling.Application;
import com.bombproofbilling.domain.Horse;
import com.bombproofbilling.repository.HorseRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the HorseResource REST controller.
 *
 * @see HorseResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class HorseResourceTest {


    private static final Long DEFAULT_HORSE_ID = 0L;
    private static final Long UPDATED_HORSE_ID = 1L;
    private static final String DEFAULT_NAME = "SAMPLE_TEXT";
    private static final String UPDATED_NAME = "UPDATED_TEXT";
    private static final String DEFAULT_BREED = "SAMPLE_TEXT";
    private static final String UPDATED_BREED = "UPDATED_TEXT";

    @Inject
    private HorseRepository horseRepository;

    private MockMvc restHorseMockMvc;

    private Horse horse;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        HorseResource horseResource = new HorseResource();
        ReflectionTestUtils.setField(horseResource, "horseRepository", horseRepository);
        this.restHorseMockMvc = MockMvcBuilders.standaloneSetup(horseResource).build();
    }

    @Before
    public void initTest() {
        horse = new Horse();
        horse.setHorseId(DEFAULT_HORSE_ID);
        horse.setName(DEFAULT_NAME);
        horse.setBreed(DEFAULT_BREED);
    }

    @Test
    @Transactional
    public void createHorse() throws Exception {
        int databaseSizeBeforeCreate = horseRepository.findAll().size();

        // Create the Horse
        restHorseMockMvc.perform(post("/api/horses")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(horse)))
                .andExpect(status().isCreated());

        // Validate the Horse in the database
        List<Horse> horses = horseRepository.findAll();
        assertThat(horses).hasSize(databaseSizeBeforeCreate + 1);
        Horse testHorse = horses.get(horses.size() - 1);
        assertThat(testHorse.getHorseId()).isEqualTo(DEFAULT_HORSE_ID);
        assertThat(testHorse.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testHorse.getBreed()).isEqualTo(DEFAULT_BREED);
    }

    @Test
    @Transactional
    public void checkHorseIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = horseRepository.findAll().size();
        // set the field null
        horse.setHorseId(null);

        // Create the Horse, which fails.
        restHorseMockMvc.perform(post("/api/horses")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(horse)))
                .andExpect(status().isBadRequest());

        List<Horse> horses = horseRepository.findAll();
        assertThat(horses).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = horseRepository.findAll().size();
        // set the field null
        horse.setName(null);

        // Create the Horse, which fails.
        restHorseMockMvc.perform(post("/api/horses")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(horse)))
                .andExpect(status().isBadRequest());

        List<Horse> horses = horseRepository.findAll();
        assertThat(horses).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllHorses() throws Exception {
        // Initialize the database
        horseRepository.saveAndFlush(horse);

        // Get all the horses
        restHorseMockMvc.perform(get("/api/horses"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(horse.getId().intValue())))
                .andExpect(jsonPath("$.[*].horseId").value(hasItem(DEFAULT_HORSE_ID.intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].breed").value(hasItem(DEFAULT_BREED.toString())));
    }

    @Test
    @Transactional
    public void getHorse() throws Exception {
        // Initialize the database
        horseRepository.saveAndFlush(horse);

        // Get the horse
        restHorseMockMvc.perform(get("/api/horses/{id}", horse.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(horse.getId().intValue()))
            .andExpect(jsonPath("$.horseId").value(DEFAULT_HORSE_ID.intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.breed").value(DEFAULT_BREED.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingHorse() throws Exception {
        // Get the horse
        restHorseMockMvc.perform(get("/api/horses/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateHorse() throws Exception {
        // Initialize the database
        horseRepository.saveAndFlush(horse);

		int databaseSizeBeforeUpdate = horseRepository.findAll().size();

        // Update the horse
        horse.setHorseId(UPDATED_HORSE_ID);
        horse.setName(UPDATED_NAME);
        horse.setBreed(UPDATED_BREED);
        restHorseMockMvc.perform(put("/api/horses")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(horse)))
                .andExpect(status().isOk());

        // Validate the Horse in the database
        List<Horse> horses = horseRepository.findAll();
        assertThat(horses).hasSize(databaseSizeBeforeUpdate);
        Horse testHorse = horses.get(horses.size() - 1);
        assertThat(testHorse.getHorseId()).isEqualTo(UPDATED_HORSE_ID);
        assertThat(testHorse.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testHorse.getBreed()).isEqualTo(UPDATED_BREED);
    }

    @Test
    @Transactional
    public void deleteHorse() throws Exception {
        // Initialize the database
        horseRepository.saveAndFlush(horse);

		int databaseSizeBeforeDelete = horseRepository.findAll().size();

        // Get the horse
        restHorseMockMvc.perform(delete("/api/horses/{id}", horse.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Horse> horses = horseRepository.findAll();
        assertThat(horses).hasSize(databaseSizeBeforeDelete - 1);
    }
}
