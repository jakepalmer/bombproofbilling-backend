package com.bombproofbilling.web.rest;

import com.bombproofbilling.Application;
import com.bombproofbilling.domain.Trainer;
import com.bombproofbilling.repository.TrainerRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the TrainerResource REST controller.
 *
 * @see TrainerResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class TrainerResourceTest {


    private static final Long DEFAULT_TRAINER_ID = 0L;
    private static final Long UPDATED_TRAINER_ID = 1L;
    private static final String DEFAULT_FIRST_NAME = "SAMPLE_TEXT";
    private static final String UPDATED_FIRST_NAME = "UPDATED_TEXT";
    private static final String DEFAULT_LAST_NAME = "SAMPLE_TEXT";
    private static final String UPDATED_LAST_NAME = "UPDATED_TEXT";
    private static final String DEFAULT_ADDRESS = "SAMPLE_TEXT";
    private static final String UPDATED_ADDRESS = "UPDATED_TEXT";

    private static final Integer DEFAULT_PHONE = 10;
    private static final Integer UPDATED_PHONE = 11;

    @Inject
    private TrainerRepository trainerRepository;

    private MockMvc restTrainerMockMvc;

    private Trainer trainer;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        TrainerResource trainerResource = new TrainerResource();
        ReflectionTestUtils.setField(trainerResource, "trainerRepository", trainerRepository);
        this.restTrainerMockMvc = MockMvcBuilders.standaloneSetup(trainerResource).build();
    }

    @Before
    public void initTest() {
        trainer = new Trainer();
        trainer.setTrainerId(DEFAULT_TRAINER_ID);
        trainer.setFirstName(DEFAULT_FIRST_NAME);
        trainer.setLastName(DEFAULT_LAST_NAME);
        trainer.setAddress(DEFAULT_ADDRESS);
        trainer.setPhone(DEFAULT_PHONE);
    }

    @Test
    @Transactional
    public void createTrainer() throws Exception {
        int databaseSizeBeforeCreate = trainerRepository.findAll().size();

        // Create the Trainer
        restTrainerMockMvc.perform(post("/api/trainers")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(trainer)))
                .andExpect(status().isCreated());

        // Validate the Trainer in the database
        List<Trainer> trainers = trainerRepository.findAll();
        assertThat(trainers).hasSize(databaseSizeBeforeCreate + 1);
        Trainer testTrainer = trainers.get(trainers.size() - 1);
        assertThat(testTrainer.getTrainerId()).isEqualTo(DEFAULT_TRAINER_ID);
        assertThat(testTrainer.getFirstName()).isEqualTo(DEFAULT_FIRST_NAME);
        assertThat(testTrainer.getLastName()).isEqualTo(DEFAULT_LAST_NAME);
        assertThat(testTrainer.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testTrainer.getPhone()).isEqualTo(DEFAULT_PHONE);
    }

    @Test
    @Transactional
    public void checkTrainerIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = trainerRepository.findAll().size();
        // set the field null
        trainer.setTrainerId(null);

        // Create the Trainer, which fails.
        restTrainerMockMvc.perform(post("/api/trainers")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(trainer)))
                .andExpect(status().isBadRequest());

        List<Trainer> trainers = trainerRepository.findAll();
        assertThat(trainers).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFirstNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = trainerRepository.findAll().size();
        // set the field null
        trainer.setFirstName(null);

        // Create the Trainer, which fails.
        restTrainerMockMvc.perform(post("/api/trainers")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(trainer)))
                .andExpect(status().isBadRequest());

        List<Trainer> trainers = trainerRepository.findAll();
        assertThat(trainers).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLastNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = trainerRepository.findAll().size();
        // set the field null
        trainer.setLastName(null);

        // Create the Trainer, which fails.
        restTrainerMockMvc.perform(post("/api/trainers")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(trainer)))
                .andExpect(status().isBadRequest());

        List<Trainer> trainers = trainerRepository.findAll();
        assertThat(trainers).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPhoneIsRequired() throws Exception {
        int databaseSizeBeforeTest = trainerRepository.findAll().size();
        // set the field null
        trainer.setPhone(null);

        // Create the Trainer, which fails.
        restTrainerMockMvc.perform(post("/api/trainers")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(trainer)))
                .andExpect(status().isBadRequest());

        List<Trainer> trainers = trainerRepository.findAll();
        assertThat(trainers).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTrainers() throws Exception {
        // Initialize the database
        trainerRepository.saveAndFlush(trainer);

        // Get all the trainers
        restTrainerMockMvc.perform(get("/api/trainers"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(trainer.getId().intValue())))
                .andExpect(jsonPath("$.[*].trainerId").value(hasItem(DEFAULT_TRAINER_ID.intValue())))
                .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME.toString())))
                .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME.toString())))
                .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS.toString())))
                .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE)));
    }

    @Test
    @Transactional
    public void getTrainer() throws Exception {
        // Initialize the database
        trainerRepository.saveAndFlush(trainer);

        // Get the trainer
        restTrainerMockMvc.perform(get("/api/trainers/{id}", trainer.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(trainer.getId().intValue()))
            .andExpect(jsonPath("$.trainerId").value(DEFAULT_TRAINER_ID.intValue()))
            .andExpect(jsonPath("$.firstName").value(DEFAULT_FIRST_NAME.toString()))
            .andExpect(jsonPath("$.lastName").value(DEFAULT_LAST_NAME.toString()))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS.toString()))
            .andExpect(jsonPath("$.phone").value(DEFAULT_PHONE));
    }

    @Test
    @Transactional
    public void getNonExistingTrainer() throws Exception {
        // Get the trainer
        restTrainerMockMvc.perform(get("/api/trainers/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTrainer() throws Exception {
        // Initialize the database
        trainerRepository.saveAndFlush(trainer);

		int databaseSizeBeforeUpdate = trainerRepository.findAll().size();

        // Update the trainer
        trainer.setTrainerId(UPDATED_TRAINER_ID);
        trainer.setFirstName(UPDATED_FIRST_NAME);
        trainer.setLastName(UPDATED_LAST_NAME);
        trainer.setAddress(UPDATED_ADDRESS);
        trainer.setPhone(UPDATED_PHONE);
        restTrainerMockMvc.perform(put("/api/trainers")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(trainer)))
                .andExpect(status().isOk());

        // Validate the Trainer in the database
        List<Trainer> trainers = trainerRepository.findAll();
        assertThat(trainers).hasSize(databaseSizeBeforeUpdate);
        Trainer testTrainer = trainers.get(trainers.size() - 1);
        assertThat(testTrainer.getTrainerId()).isEqualTo(UPDATED_TRAINER_ID);
        assertThat(testTrainer.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
        assertThat(testTrainer.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testTrainer.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testTrainer.getPhone()).isEqualTo(UPDATED_PHONE);
    }

    @Test
    @Transactional
    public void deleteTrainer() throws Exception {
        // Initialize the database
        trainerRepository.saveAndFlush(trainer);

		int databaseSizeBeforeDelete = trainerRepository.findAll().size();

        // Get the trainer
        restTrainerMockMvc.perform(delete("/api/trainers/{id}", trainer.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Trainer> trainers = trainerRepository.findAll();
        assertThat(trainers).hasSize(databaseSizeBeforeDelete - 1);
    }
}
