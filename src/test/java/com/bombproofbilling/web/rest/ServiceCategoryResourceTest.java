package com.bombproofbilling.web.rest;

import com.bombproofbilling.Application;
import com.bombproofbilling.domain.ServiceCategory;
import com.bombproofbilling.repository.ServiceCategoryRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the ServiceCategoryResource REST controller.
 *
 * @see ServiceCategoryResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class ServiceCategoryResourceTest {

    private static final String DEFAULT_NAME = "SAMPLE_TEXT";
    private static final String UPDATED_NAME = "UPDATED_TEXT";

    @Inject
    private ServiceCategoryRepository serviceCategoryRepository;

    private MockMvc restServiceCategoryMockMvc;

    private ServiceCategory serviceCategory;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ServiceCategoryResource serviceCategoryResource = new ServiceCategoryResource();
        ReflectionTestUtils.setField(serviceCategoryResource, "serviceCategoryRepository", serviceCategoryRepository);
        this.restServiceCategoryMockMvc = MockMvcBuilders.standaloneSetup(serviceCategoryResource).build();
    }

    @Before
    public void initTest() {
        serviceCategory = new ServiceCategory();
        serviceCategory.setName(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createServiceCategory() throws Exception {
        int databaseSizeBeforeCreate = serviceCategoryRepository.findAll().size();

        // Create the ServiceCategory
        restServiceCategoryMockMvc.perform(post("/api/serviceCategorys")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(serviceCategory)))
                .andExpect(status().isCreated());

        // Validate the ServiceCategory in the database
        List<ServiceCategory> serviceCategorys = serviceCategoryRepository.findAll();
        assertThat(serviceCategorys).hasSize(databaseSizeBeforeCreate + 1);
        ServiceCategory testServiceCategory = serviceCategorys.get(serviceCategorys.size() - 1);
        assertThat(testServiceCategory.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = serviceCategoryRepository.findAll().size();
        // set the field null
        serviceCategory.setName(null);

        // Create the ServiceCategory, which fails.
        restServiceCategoryMockMvc.perform(post("/api/serviceCategorys")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(serviceCategory)))
                .andExpect(status().isBadRequest());

        List<ServiceCategory> serviceCategorys = serviceCategoryRepository.findAll();
        assertThat(serviceCategorys).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllServiceCategorys() throws Exception {
        // Initialize the database
        serviceCategoryRepository.saveAndFlush(serviceCategory);

        // Get all the serviceCategorys
        restServiceCategoryMockMvc.perform(get("/api/serviceCategorys"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(serviceCategory.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void getServiceCategory() throws Exception {
        // Initialize the database
        serviceCategoryRepository.saveAndFlush(serviceCategory);

        // Get the serviceCategory
        restServiceCategoryMockMvc.perform(get("/api/serviceCategorys/{id}", serviceCategory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(serviceCategory.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingServiceCategory() throws Exception {
        // Get the serviceCategory
        restServiceCategoryMockMvc.perform(get("/api/serviceCategorys/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateServiceCategory() throws Exception {
        // Initialize the database
        serviceCategoryRepository.saveAndFlush(serviceCategory);

		int databaseSizeBeforeUpdate = serviceCategoryRepository.findAll().size();

        // Update the serviceCategory
        serviceCategory.setName(UPDATED_NAME);
        restServiceCategoryMockMvc.perform(put("/api/serviceCategorys")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(serviceCategory)))
                .andExpect(status().isOk());

        // Validate the ServiceCategory in the database
        List<ServiceCategory> serviceCategorys = serviceCategoryRepository.findAll();
        assertThat(serviceCategorys).hasSize(databaseSizeBeforeUpdate);
        ServiceCategory testServiceCategory = serviceCategorys.get(serviceCategorys.size() - 1);
        assertThat(testServiceCategory.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void deleteServiceCategory() throws Exception {
        // Initialize the database
        serviceCategoryRepository.saveAndFlush(serviceCategory);

		int databaseSizeBeforeDelete = serviceCategoryRepository.findAll().size();

        // Get the serviceCategory
        restServiceCategoryMockMvc.perform(delete("/api/serviceCategorys/{id}", serviceCategory.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<ServiceCategory> serviceCategorys = serviceCategoryRepository.findAll();
        assertThat(serviceCategorys).hasSize(databaseSizeBeforeDelete - 1);
    }
}
