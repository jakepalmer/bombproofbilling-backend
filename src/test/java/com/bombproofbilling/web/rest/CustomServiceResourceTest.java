package com.bombproofbilling.web.rest;

import com.bombproofbilling.Application;
import com.bombproofbilling.domain.CustomService;
import com.bombproofbilling.repository.CustomServiceRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the CustomServiceResource REST controller.
 *
 * @see CustomServiceResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class CustomServiceResourceTest {

    private static final String DEFAULT_NAME = "SAMPLE_TEXT";
    private static final String UPDATED_NAME = "UPDATED_TEXT";

    private static final BigDecimal DEFAULT_COST = new BigDecimal(0);
    private static final BigDecimal UPDATED_COST = new BigDecimal(1);

    @Inject
    private CustomServiceRepository customServiceRepository;

    private MockMvc restCustomServiceMockMvc;

    private CustomService customService;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        CustomServiceResource customServiceResource = new CustomServiceResource();
        ReflectionTestUtils.setField(customServiceResource, "customServiceRepository", customServiceRepository);
        this.restCustomServiceMockMvc = MockMvcBuilders.standaloneSetup(customServiceResource).build();
    }

    @Before
    public void initTest() {
        customService = new CustomService();
        customService.setName(DEFAULT_NAME);
        customService.setCost(DEFAULT_COST);
    }

    @Test
    @Transactional
    public void createCustomService() throws Exception {
        int databaseSizeBeforeCreate = customServiceRepository.findAll().size();

        // Create the CustomService
        restCustomServiceMockMvc.perform(post("/api/customServices")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(customService)))
                .andExpect(status().isCreated());

        // Validate the CustomService in the database
        List<CustomService> customServices = customServiceRepository.findAll();
        assertThat(customServices).hasSize(databaseSizeBeforeCreate + 1);
        CustomService testCustomService = customServices.get(customServices.size() - 1);
        assertThat(testCustomService.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCustomService.getCost()).isEqualTo(DEFAULT_COST);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = customServiceRepository.findAll().size();
        // set the field null
        customService.setName(null);

        // Create the CustomService, which fails.
        restCustomServiceMockMvc.perform(post("/api/customServices")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(customService)))
                .andExpect(status().isBadRequest());

        List<CustomService> customServices = customServiceRepository.findAll();
        assertThat(customServices).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCostIsRequired() throws Exception {
        int databaseSizeBeforeTest = customServiceRepository.findAll().size();
        // set the field null
        customService.setCost(null);

        // Create the CustomService, which fails.
        restCustomServiceMockMvc.perform(post("/api/customServices")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(customService)))
                .andExpect(status().isBadRequest());

        List<CustomService> customServices = customServiceRepository.findAll();
        assertThat(customServices).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCustomServices() throws Exception {
        // Initialize the database
        customServiceRepository.saveAndFlush(customService);

        // Get all the customServices
        restCustomServiceMockMvc.perform(get("/api/customServices"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(customService.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].cost").value(hasItem(DEFAULT_COST.intValue())));
    }

    @Test
    @Transactional
    public void getCustomService() throws Exception {
        // Initialize the database
        customServiceRepository.saveAndFlush(customService);

        // Get the customService
        restCustomServiceMockMvc.perform(get("/api/customServices/{id}", customService.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(customService.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.cost").value(DEFAULT_COST.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingCustomService() throws Exception {
        // Get the customService
        restCustomServiceMockMvc.perform(get("/api/customServices/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCustomService() throws Exception {
        // Initialize the database
        customServiceRepository.saveAndFlush(customService);

		int databaseSizeBeforeUpdate = customServiceRepository.findAll().size();

        // Update the customService
        customService.setName(UPDATED_NAME);
        customService.setCost(UPDATED_COST);
        restCustomServiceMockMvc.perform(put("/api/customServices")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(customService)))
                .andExpect(status().isOk());

        // Validate the CustomService in the database
        List<CustomService> customServices = customServiceRepository.findAll();
        assertThat(customServices).hasSize(databaseSizeBeforeUpdate);
        CustomService testCustomService = customServices.get(customServices.size() - 1);
        assertThat(testCustomService.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCustomService.getCost()).isEqualTo(UPDATED_COST);
    }

    @Test
    @Transactional
    public void deleteCustomService() throws Exception {
        // Initialize the database
        customServiceRepository.saveAndFlush(customService);

		int databaseSizeBeforeDelete = customServiceRepository.findAll().size();

        // Get the customService
        restCustomServiceMockMvc.perform(delete("/api/customServices/{id}", customService.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<CustomService> customServices = customServiceRepository.findAll();
        assertThat(customServices).hasSize(databaseSizeBeforeDelete - 1);
    }
}
