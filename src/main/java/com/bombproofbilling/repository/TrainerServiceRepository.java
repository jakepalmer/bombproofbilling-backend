package com.bombproofbilling.repository;

import com.bombproofbilling.domain.TrainerService;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the TrainerService entity.
 */
public interface TrainerServiceRepository extends JpaRepository<TrainerService,Long> {

}
