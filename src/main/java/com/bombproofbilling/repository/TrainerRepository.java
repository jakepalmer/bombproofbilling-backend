package com.bombproofbilling.repository;

import com.bombproofbilling.domain.Trainer;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Trainer entity.
 */
public interface TrainerRepository extends JpaRepository<Trainer,Long> {

    @Query("select trainer from Trainer trainer where trainer.user.login = ?#{principal.username}")
    List<Trainer> findAllForCurrentUser();

}
