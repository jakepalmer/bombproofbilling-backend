package com.bombproofbilling.web.rest;

import com.bombproofbilling.domain.Trainer;
import com.bombproofbilling.repository.TrainerRepository;
import com.codahale.metrics.annotation.Timed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

/**
 * REST controller for managing Trainer.
 */
@RestController
@RequestMapping("/api")
public class TrainerResource {

    private final Logger log = LoggerFactory.getLogger(TrainerResource.class);

    @Inject
    private TrainerRepository trainerRepository;

    /**
     * POST  /trainers -> Create a new trainer.
     */
    @RequestMapping(value = "/trainers",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Trainer> create(@Valid @RequestBody Trainer trainer) throws URISyntaxException {
        log.debug("REST request to save Trainer : {}", trainer);
        if (trainer.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new trainer cannot already have an ID").body(null);
        }
        Trainer result = trainerRepository.save(trainer);
        return ResponseEntity.created(new URI("/api/trainers/" + trainer.getId())).body(result);
    }

    /**
     * PUT  /trainers -> Updates an existing trainer.
     */
    @RequestMapping(value = "/trainers",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Trainer> update(@Valid @RequestBody Trainer trainer) throws URISyntaxException {
        log.debug("REST request to update Trainer : {}", trainer);
        if (trainer.getId() == null) {
            return create(trainer);
        }
        Trainer result = trainerRepository.save(trainer);
        return ResponseEntity.ok().body(result);
    }

    /**
     * GET  /trainers -> get all the trainers.
     */
    @RequestMapping(value = "/trainers",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Trainer> getAll() {
        log.debug("REST request to get all Trainers");
        return trainerRepository.findAll();
    }

    /**
     * GET  /trainers/:id -> get the "id" trainer.
     */
    @RequestMapping(value = "/trainers/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Trainer> get(@PathVariable Long id, HttpServletResponse response) {
        log.debug("REST request to get Trainer : {}", id);
        Trainer trainer = trainerRepository.findOne(id);
        if (trainer == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(trainer, HttpStatus.OK);
    }

    /**
     * DELETE  /trainers/:id -> delete the "id" trainer.
     */
    @RequestMapping(value = "/trainers/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete Trainer : {}", id);
        trainerRepository.delete(id);
    }
}
