package com.bombproofbilling.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.bombproofbilling.domain.Invoice;
import com.bombproofbilling.repository.InvoiceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * REST controller for managing Invoice.
 */
@RestController
@RequestMapping("/api")
public class InvoiceResource {

    private final Logger log = LoggerFactory.getLogger(InvoiceResource.class);

    @Inject
    private InvoiceRepository invoiceRepository;

    /**
     * POST  /invoices -> Create a new invoice.
     */
    @RequestMapping(value = "/invoices",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Invoice> create(@Valid @RequestBody Invoice invoice) throws URISyntaxException {
        log.debug("REST request to save Invoice : {}", invoice);
        if (invoice.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new invoice cannot already have an ID").body(null);
        }
        Invoice result = invoiceRepository.save(invoice);
        return ResponseEntity.created(new URI("/api/invoices/" + invoice.getId())).body(result);
    }

    /**
     * PUT  /invoices -> Updates an existing invoice.
     */
    @RequestMapping(value = "/invoices",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Invoice> update(@Valid @RequestBody Invoice invoice) throws URISyntaxException {
        log.debug("REST request to update Invoice : {}", invoice);
        if (invoice.getId() == null) {
            return create(invoice);
        }
        Invoice result = invoiceRepository.save(invoice);
        return ResponseEntity.ok().body(result);
    }

    /**
     * GET  /invoices -> get all the invoices.
     */
    @RequestMapping(value = "/invoices",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Invoice> getAll() {
        log.debug("REST request to get all Invoices");
        return invoiceRepository.findAll();
    }

    /**
     * GET  /invoices/:id -> get the "id" invoice.
     */
    @RequestMapping(value = "/invoices/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Invoice> get(@PathVariable Long id, HttpServletResponse response) {
        log.debug("REST request to get Invoice : {}", id);
        Invoice invoice = invoiceRepository.findOne(id);
        if (invoice == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(invoice, HttpStatus.OK);
    }

    /**
     * DELETE  /invoices/:id -> delete the "id" invoice.
     */
    @RequestMapping(value = "/invoices/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete Invoice : {}", id);
        invoiceRepository.delete(id);
    }
}
