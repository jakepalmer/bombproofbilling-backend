package com.bombproofbilling.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.bombproofbilling.domain.ServiceCategory;
import com.bombproofbilling.repository.ServiceCategoryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * REST controller for managing ServiceCategory.
 */
@RestController
@RequestMapping("/api")
public class ServiceCategoryResource {

    private final Logger log = LoggerFactory.getLogger(ServiceCategoryResource.class);

    @Inject
    private ServiceCategoryRepository serviceCategoryRepository;

    /**
     * POST  /serviceCategorys -> Create a new serviceCategory.
     */
    @RequestMapping(value = "/serviceCategorys",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ServiceCategory> create(@Valid @RequestBody ServiceCategory serviceCategory) throws URISyntaxException {
        log.debug("REST request to save ServiceCategory : {}", serviceCategory);
        if (serviceCategory.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new serviceCategory cannot already have an ID").body(null);
        }
        ServiceCategory result = serviceCategoryRepository.save(serviceCategory);
        return ResponseEntity.created(new URI("/api/serviceCategorys/" + serviceCategory.getId())).body(result);
    }

    /**
     * PUT  /serviceCategorys -> Updates an existing serviceCategory.
     */
    @RequestMapping(value = "/serviceCategorys",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ServiceCategory> update(@Valid @RequestBody ServiceCategory serviceCategory) throws URISyntaxException {
        log.debug("REST request to update ServiceCategory : {}", serviceCategory);
        if (serviceCategory.getId() == null) {
            return create(serviceCategory);
        }
        ServiceCategory result = serviceCategoryRepository.save(serviceCategory);
        return ResponseEntity.ok().body(result);
    }

    /**
     * GET  /serviceCategorys -> get all the serviceCategorys.
     */
    @RequestMapping(value = "/serviceCategorys",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<ServiceCategory> getAll() {
        log.debug("REST request to get all ServiceCategorys");
        return serviceCategoryRepository.findAll();
    }

    /**
     * GET  /serviceCategorys/:id -> get the "id" serviceCategory.
     */
    @RequestMapping(value = "/serviceCategorys/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ServiceCategory> get(@PathVariable Long id, HttpServletResponse response) {
        log.debug("REST request to get ServiceCategory : {}", id);
        ServiceCategory serviceCategory = serviceCategoryRepository.findOne(id);
        if (serviceCategory == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(serviceCategory, HttpStatus.OK);
    }

    /**
     * DELETE  /serviceCategorys/:id -> delete the "id" serviceCategory.
     */
    @RequestMapping(value = "/serviceCategorys/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete ServiceCategory : {}", id);
        serviceCategoryRepository.delete(id);
    }
}
