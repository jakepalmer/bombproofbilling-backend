package com.bombproofbilling.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.bombproofbilling.domain.TrainerService;
import com.bombproofbilling.repository.TrainerServiceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * REST controller for managing TrainerService.
 */
@RestController
@RequestMapping("/api")
public class TrainerServiceResource {

    private final Logger log = LoggerFactory.getLogger(TrainerServiceResource.class);

    @Inject
    private TrainerServiceRepository trainerServiceRepository;

    /**
     * POST  /trainerServices -> Create a new trainerService.
     */
    @RequestMapping(value = "/trainerServices",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TrainerService> create(@Valid @RequestBody TrainerService trainerService) throws URISyntaxException {
        log.debug("REST request to save TrainerService : {}", trainerService);
        if (trainerService.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new trainerService cannot already have an ID").body(null);
        }
        TrainerService result = trainerServiceRepository.save(trainerService);
        return ResponseEntity.created(new URI("/api/trainerServices/" + trainerService.getId())).body(result);
    }

    /**
     * PUT  /trainerServices -> Updates an existing trainerService.
     */
    @RequestMapping(value = "/trainerServices",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TrainerService> update(@Valid @RequestBody TrainerService trainerService) throws URISyntaxException {
        log.debug("REST request to update TrainerService : {}", trainerService);
        if (trainerService.getId() == null) {
            return create(trainerService);
        }
        TrainerService result = trainerServiceRepository.save(trainerService);
        return ResponseEntity.ok().body(result);
    }

    /**
     * GET  /trainerServices -> get all the trainerServices.
     */
    @RequestMapping(value = "/trainerServices",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<TrainerService> getAll() {
        log.debug("REST request to get all TrainerServices");
        return trainerServiceRepository.findAll();
    }

    /**
     * GET  /trainerServices/:id -> get the "id" trainerService.
     */
    @RequestMapping(value = "/trainerServices/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TrainerService> get(@PathVariable Long id, HttpServletResponse response) {
        log.debug("REST request to get TrainerService : {}", id);
        TrainerService trainerService = trainerServiceRepository.findOne(id);
        if (trainerService == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(trainerService, HttpStatus.OK);
    }

    /**
     * DELETE  /trainerServices/:id -> delete the "id" trainerService.
     */
    @RequestMapping(value = "/trainerServices/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete TrainerService : {}", id);
        trainerServiceRepository.delete(id);
    }
}
