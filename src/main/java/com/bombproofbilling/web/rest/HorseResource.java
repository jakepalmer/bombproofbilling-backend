package com.bombproofbilling.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.bombproofbilling.domain.Horse;
import com.bombproofbilling.repository.HorseRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * REST controller for managing Horse.
 */
@RestController
@RequestMapping("/api")
public class HorseResource {

    private final Logger log = LoggerFactory.getLogger(HorseResource.class);

    @Inject
    private HorseRepository horseRepository;

    /**
     * POST  /horses -> Create a new horse.
     */
    @RequestMapping(value = "/horses",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Horse> create(@Valid @RequestBody Horse horse) throws URISyntaxException {
        log.debug("REST request to save Horse : {}", horse);
        if (horse.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new horse cannot already have an ID").body(null);
        }
        Horse result = horseRepository.save(horse);
        return ResponseEntity.created(new URI("/api/horses/" + horse.getId())).body(result);
    }

    /**
     * PUT  /horses -> Updates an existing horse.
     */
    @RequestMapping(value = "/horses",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Horse> update(@Valid @RequestBody Horse horse) throws URISyntaxException {
        log.debug("REST request to update Horse : {}", horse);
        if (horse.getId() == null) {
            return create(horse);
        }
        Horse result = horseRepository.save(horse);
        return ResponseEntity.ok().body(result);
    }

    /**
     * GET  /horses -> get all the horses.
     */
    @RequestMapping(value = "/horses",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Horse> getAll() {
        log.debug("REST request to get all Horses");
        return horseRepository.findAll();
    }

    /**
     * GET  /horses/:id -> get the "id" horse.
     */
    @RequestMapping(value = "/horses/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Horse> get(@PathVariable Long id, HttpServletResponse response) {
        log.debug("REST request to get Horse : {}", id);
        Horse horse = horseRepository.findOneWithEagerRelationships(id);
        if (horse == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(horse, HttpStatus.OK);
    }

    /**
     * DELETE  /horses/:id -> delete the "id" horse.
     */
    @RequestMapping(value = "/horses/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete Horse : {}", id);
        horseRepository.delete(id);
    }
}
