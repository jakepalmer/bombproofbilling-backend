package com.bombproofbilling.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.bombproofbilling.domain.Service;
import com.bombproofbilling.repository.ServiceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * REST controller for managing Service.
 */
@RestController
@RequestMapping("/api")
public class ServiceResource {

    private final Logger log = LoggerFactory.getLogger(ServiceResource.class);

    @Inject
    private ServiceRepository serviceRepository;

    /**
     * POST  /services -> Create a new service.
     */
    @RequestMapping(value = "/services",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Service> create(@Valid @RequestBody Service service) throws URISyntaxException {
        log.debug("REST request to save Service : {}", service);
        if (service.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new service cannot already have an ID").body(null);
        }
        Service result = serviceRepository.save(service);
        return ResponseEntity.created(new URI("/api/services/" + service.getId())).body(result);
    }

    /**
     * PUT  /services -> Updates an existing service.
     */
    @RequestMapping(value = "/services",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Service> update(@Valid @RequestBody Service service) throws URISyntaxException {
        log.debug("REST request to update Service : {}", service);
        if (service.getId() == null) {
            return create(service);
        }
        Service result = serviceRepository.save(service);
        return ResponseEntity.ok().body(result);
    }

    /**
     * GET  /services -> get all the services.
     */
    @RequestMapping(value = "/services",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Service> getAll() {
        log.debug("REST request to get all Services");
        return serviceRepository.findAll();
    }

    /**
     * GET  /services/:id -> get the "id" service.
     */
    @RequestMapping(value = "/services/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Service> get(@PathVariable Long id, HttpServletResponse response) {
        log.debug("REST request to get Service : {}", id);
        Service service = serviceRepository.findOne(id);
        if (service == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(service, HttpStatus.OK);
    }

    /**
     * DELETE  /services/:id -> delete the "id" service.
     */
    @RequestMapping(value = "/services/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete Service : {}", id);
        serviceRepository.delete(id);
    }
}
